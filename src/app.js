const fs = require('fs');
const path = require('path');

/* Create path to inputs directory, join and target specfic input file */
const inputPath = require('path').dirname('/inputs/input-1.txt');
const inputFile = path.join(__dirname, inputPath + '/input-1.txt');

function outputWords() {
  /* Read input file and print output to command line */
  fs.readFile(inputFile, 'utf-8', (err, data) => {
    if (err) {
      console.log(err);
      return;
    } else {
      const arrChar = data.split('');
      const gridNumsPositionLength = arrChar.indexOf('\n');
      const arrRowByColumn = [];

      for (let i = 0; i < gridNumsPositionLength; i++) {
        arrRowByColumn.push(arrChar[i]);
      }
      // console.log(arrRowByColumn);
      const byPosition = arrRowByColumn.indexOf('x');

      /*
      Use slice method to extract row number and column number
      Convert number string data type to number with parseInt()
      */
      const rowNumber = parseInt(arrRowByColumn.join('').slice(0, byPosition));
      const columnNumber = parseInt(
        arrRowByColumn.join('').slice(byPosition + 1, arrRowByColumn.length)
      );
      console.log(`Row: ${rowNumber}`);
      console.log(`Column: ${columnNumber}`);
      // console.log(arrChar.join('').split('\n'));
      const splitDataLineBreak = arrChar.join('').split('\n');
      const arrGridLetters = [];

      /* Use column number as condition and add 1*/
      for (let i = 1; i < columnNumber + 1; i++) {
        arrGridLetters.push(splitDataLineBreak[i]);
      }
      console.log(arrGridLetters);

      /* Create a 2-D grid */
      const twoDimensionalGrid = [];
      for (let i = 0; i < arrGridLetters.length; i++) {
        twoDimensionalGrid.push(arrGridLetters[i].split(' '));
      }

      console.log(twoDimensionalGrid);
    }
  });
}

outputWords();
